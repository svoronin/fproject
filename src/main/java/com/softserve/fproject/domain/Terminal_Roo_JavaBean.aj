// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.softserve.fproject.domain;

import com.softserve.fproject.domain.Airport;
import com.softserve.fproject.domain.TerminalClass;

privileged aspect Terminal_Roo_JavaBean {
    
    public TerminalClass Terminal.getTerminalName() {
        return this.terminalName;
    }
    
    public void Terminal.setTerminalName(TerminalClass terminalName) {
        this.terminalName = terminalName;
    }
    
    public Airport Terminal.getAirport() {
        return this.airport;
    }
    
    public void Terminal.setAirport(Airport airport) {
        this.airport = airport;
    }
    
}

package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.softserve.fproject.domain.Country;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooEntity
public class City {

    @NotNull
    @Size(max = 30)
    private String name;

    @NotNull
    @ManyToOne
    private Country country;
}

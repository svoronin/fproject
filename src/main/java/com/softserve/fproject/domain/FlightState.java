package com.softserve.fproject.domain;


public enum FlightState {

    Arrived, RegistrationIsOpenned, RegistrationIsClosed, AlreadyLeft, Cancelled
}

package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import com.softserve.fproject.domain.Airport;
import javax.validation.constraints.NotNull;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;

@RooJavaBean
@RooToString
@RooEntity
public class Hop {

    @NotNull
    @ManyToOne
    private Airport fromAirport;

    @NotNull
    @ManyToOne
    private Airport toAirport;

    @NotNull
    @Digits(integer = 2, fraction = 1)
    private Float duration;
}

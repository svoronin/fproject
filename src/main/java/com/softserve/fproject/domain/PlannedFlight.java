package com.softserve.fproject.domain;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import java.util.Set;
import com.softserve.fproject.domain.Hop;
import java.util.HashSet;
import javax.validation.constraints.NotNull;
import javax.persistence.ManyToMany;
import javax.persistence.CascadeType;
import com.softserve.fproject.domain.Plane;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;

@RooJavaBean
@RooToString
@RooEntity
public class PlannedFlight {

    @NotNull
    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Hop> hops = new HashSet<Hop>();

    @NotNull
    @ManyToOne
    private Plane plane;

    @NotNull
    @Digits(integer = 8, fraction = 2)
    private Double firstClassSitsCost;

    @NotNull
    @Digits(integer = 8, fraction = 2)
    private Double secondClassSitsCost;
}

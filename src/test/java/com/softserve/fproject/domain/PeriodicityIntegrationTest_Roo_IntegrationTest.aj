// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.softserve.fproject.domain;

import com.softserve.fproject.domain.PeriodicityDataOnDemand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

privileged aspect PeriodicityIntegrationTest_Roo_IntegrationTest {
    
    declare @type: PeriodicityIntegrationTest: @RunWith(SpringJUnit4ClassRunner.class);
    
    declare @type: PeriodicityIntegrationTest: @ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext.xml");
    
    declare @type: PeriodicityIntegrationTest: @Transactional;
    
    @Autowired
    private PeriodicityDataOnDemand PeriodicityIntegrationTest.dod;
    
    @Test
    public void PeriodicityIntegrationTest.testCountPeriodicitys() {
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to initialize correctly", dod.getRandomPeriodicity());
        long count = com.softserve.fproject.domain.Periodicity.countPeriodicitys();
        org.junit.Assert.assertTrue("Counter for 'Periodicity' incorrectly reported there were no entries", count > 0);
    }
    
    @Test
    public void PeriodicityIntegrationTest.testFindPeriodicity() {
        com.softserve.fproject.domain.Periodicity obj = dod.getRandomPeriodicity();
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to provide an identifier", id);
        obj = com.softserve.fproject.domain.Periodicity.findPeriodicity(id);
        org.junit.Assert.assertNotNull("Find method for 'Periodicity' illegally returned null for id '" + id + "'", obj);
        org.junit.Assert.assertEquals("Find method for 'Periodicity' returned the incorrect identifier", id, obj.getId());
    }
    
    @Test
    public void PeriodicityIntegrationTest.testFindAllPeriodicitys() {
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to initialize correctly", dod.getRandomPeriodicity());
        long count = com.softserve.fproject.domain.Periodicity.countPeriodicitys();
        org.junit.Assert.assertTrue("Too expensive to perform a find all test for 'Periodicity', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        java.util.List<com.softserve.fproject.domain.Periodicity> result = com.softserve.fproject.domain.Periodicity.findAllPeriodicitys();
        org.junit.Assert.assertNotNull("Find all method for 'Periodicity' illegally returned null", result);
        org.junit.Assert.assertTrue("Find all method for 'Periodicity' failed to return any data", result.size() > 0);
    }
    
    @Test
    public void PeriodicityIntegrationTest.testFindPeriodicityEntries() {
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to initialize correctly", dod.getRandomPeriodicity());
        long count = com.softserve.fproject.domain.Periodicity.countPeriodicitys();
        if (count > 20) count = 20;
        java.util.List<com.softserve.fproject.domain.Periodicity> result = com.softserve.fproject.domain.Periodicity.findPeriodicityEntries(0, (int) count);
        org.junit.Assert.assertNotNull("Find entries method for 'Periodicity' illegally returned null", result);
        org.junit.Assert.assertEquals("Find entries method for 'Periodicity' returned an incorrect number of entries", count, result.size());
    }
    
    @Test
    public void PeriodicityIntegrationTest.testFlush() {
        com.softserve.fproject.domain.Periodicity obj = dod.getRandomPeriodicity();
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to provide an identifier", id);
        obj = com.softserve.fproject.domain.Periodicity.findPeriodicity(id);
        org.junit.Assert.assertNotNull("Find method for 'Periodicity' illegally returned null for id '" + id + "'", obj);
        boolean modified =  dod.modifyPeriodicity(obj);
        java.lang.Integer currentVersion = obj.getVersion();
        obj.flush();
        org.junit.Assert.assertTrue("Version for 'Periodicity' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void PeriodicityIntegrationTest.testMerge() {
        com.softserve.fproject.domain.Periodicity obj = dod.getRandomPeriodicity();
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to provide an identifier", id);
        obj = com.softserve.fproject.domain.Periodicity.findPeriodicity(id);
        boolean modified =  dod.modifyPeriodicity(obj);
        java.lang.Integer currentVersion = obj.getVersion();
        com.softserve.fproject.domain.Periodicity merged = (com.softserve.fproject.domain.Periodicity) obj.merge();
        obj.flush();
        org.junit.Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        org.junit.Assert.assertTrue("Version for 'Periodicity' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void PeriodicityIntegrationTest.testPersist() {
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to initialize correctly", dod.getRandomPeriodicity());
        com.softserve.fproject.domain.Periodicity obj = dod.getNewTransientPeriodicity(Integer.MAX_VALUE);
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to provide a new transient entity", obj);
        org.junit.Assert.assertNull("Expected 'Periodicity' identifier to be null", obj.getId());
        obj.persist();
        obj.flush();
        org.junit.Assert.assertNotNull("Expected 'Periodicity' identifier to no longer be null", obj.getId());
    }
    
    @Test
    public void PeriodicityIntegrationTest.testRemove() {
        com.softserve.fproject.domain.Periodicity obj = dod.getRandomPeriodicity();
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to initialize correctly", obj);
        java.lang.Long id = obj.getId();
        org.junit.Assert.assertNotNull("Data on demand for 'Periodicity' failed to provide an identifier", id);
        obj = com.softserve.fproject.domain.Periodicity.findPeriodicity(id);
        obj.remove();
        obj.flush();
        org.junit.Assert.assertNull("Failed to remove 'Periodicity' with identifier '" + id + "'", com.softserve.fproject.domain.Periodicity.findPeriodicity(id));
    }
    
}
